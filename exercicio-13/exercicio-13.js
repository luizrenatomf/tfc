(function() {
    const turma = [];
    let nome, nota;
    function criaAluno (nome, nota) {
        return {
            nome,
            nota,
        };
    }

    do {
        nome = prompt("Insira o nome do aluno");
        do {
            nota = Number(prompt(`Insira a nota do aluno ${nome}.`));
        } while(nota < 0 || nota > 100)
        turma.push(new criaAluno(nome,nota));
    } while(window.confirm("Deseja cadastrar mais alunos?"));

    const reprovados = turma.filter(function(aluno) {
        return aluno.nota < 60;
    });

    console.log("ALUNOS REPROVADOS");
    reprovados.forEach(function(aluno) {
        console.log(`Aluno: ${aluno.nome}, nota: ${aluno.nota}`)
    })
}());