let notaAluno1 = Number(prompt("Informe a nota do primeiro aluno:"));
let notaAluno2 = Number(prompt("Informe a nota do segundo aluno:"));
let notaAluno3 = Number(prompt("Informe a nota do terceiro aluno:"));

let mediaFinalAlunos = (notaAluno1 + notaAluno2 + notaAluno3) / 3;

console.log(`O aluno 1 teve um aproveitamento de: ${notaAluno1}`);
console.log(`O aluno 2 teve um aproveitamento de: ${notaAluno2}`);
console.log(`O aluno 3 teve um aproveitamento de: ${notaAluno3}`);
console.log(`A média da nota final de todos os alunos que frequentaram o presencial foi de: ${mediaFinalAlunos}`);