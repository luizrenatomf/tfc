(function() {
    let w;
    const asc = () => {
        for(let i = 1; i <= 10; i++) {
            console.log(i);
        }
    }

    const desc = () => {
        for(let i = 10; i >= 1; i--) {
            console.log(i);
        }
    }

    const inter = () => {
        for(let i = 1; i <= 5; i++) {
            console.log(i);
            console.log(`${11-i}`);
        }
    }

    do {
        w = (prompt("Escolha entre ASC, DESC ou INTER:")).trim().toLocaleLowerCase();
        if(w === 'asc') asc();
        if(w === 'desc') desc();
        if(w === 'inter') inter();

        if(w != 'asc' && w != 'desc' && w != 'inter') {
            console.log("Erro ao escolher opção, tente novamente.");
        }
    } while(w != 'asc' && w != 'desc' && w != 'inter');

}());