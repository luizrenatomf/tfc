(function() {
    let turma = [];
    let nome, nota, bonus;
    function criaAluno (nome, nota, bonus) {
        return {
            nome,
            nota,
            bonus,
        };
    }

    do {
        nome = prompt("Insira o nome do aluno");
        do {
            nota = Number(prompt(`Insira a nota do aluno ${nome}.`));
        } while(nota < 0 || nota > 100)
        bonus = window.confirm("O aluno pontuação bônus?");
        turma.push(new criaAluno(nome,nota,bonus));
    } while(window.confirm("Deseja cadastrar mais alunos?"));

    turma = turma.map(function(aluno) {
        if(aluno.nota >= 55 && aluno.nota < 60) {
            if(aluno.bonus === true) {
                aluno.nota = 60;
            }
        } else if (aluno.nota >= 60 && aluno.bonus === true) {
            if(aluno.nota * 1.1 > 100) {
                aluno.nota = 100;
            } else {
                aluno.nota = aluno.nota * 1.1;
            }
        }
        return aluno;
    });

    console.log("ALUNOS");
    turma.forEach(function(aluno) {
        console.log(`Aluno: ${aluno.nome}, nota: ${aluno.nota}, bônus: ${aluno.bonus ? 'Possui' : 'Não possui'}`)
    });

    const reprovados = turma.filter(function(aluno) {
        return aluno.nota < 60;
    });

    console.log("ALUNOS REPROVADOS");
    reprovados.forEach(function(aluno) {
        console.log(`Aluno: ${aluno.nome}, nota: ${aluno.nota}`)
    });
}());