(function() {
    let a, b, w;
    const sum = (a, b) => {
        let total = 0;;
        for(let aux = a; aux <= b; aux++) {
            total = total + aux;
        }
        console.log(`SUM[${a},${b}] = ${total}`);
    };
    const mul = (a, b) => {
        let total = a;
        for(let aux = a+1; aux <= b; aux++) {
            total = total * aux;
        }
        console.log(`MUL[${a},${b}] = ${total}`);
    };
    const div = (a, b) => {
        let total = a;
        for(let aux = a+1; aux <= b; aux++) {
            total = total / aux;
        }
        console.log(`DIV[${a},${b}] = ${total}`);
    };

    do {
        a = Number(prompt("Entrada 1"));
        b = Number(prompt("Entrada 2"));
        w = prompt("Entrada 3").trim().toLocaleLowerCase();

        if(w != 'sum' && w != 'mul' && w != 'div') {
            console.log(`Função ${w} não existe.`);
        } else if(isNaN(a) || isNaN(b)) {
            console.log("Número inválido. Digite novamente.");
        } else if(w === 'sum') {
            if(a > b) {
                console.log("O valor A deve ser menor que B.");
            } else {
                sum(a, b);
            }
        } else if(w === 'mul') { mul(a, b) 
        } else if(w === 'div') {
            if(b == 0) {
                console.log("Não é possível realizar divisão com denominador zero.")
            } else {
                div(a, b);
            }
        }
    } while(w != 'sum' && w != 'mul' && w != 'div');
}());