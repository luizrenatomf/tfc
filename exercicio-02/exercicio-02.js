let num1 = Number(prompt("Insira o primeiro valor:"));
let num2 = Number(prompt("Insira o segundo valor:"));
let soma = num1 + num2
let subtracao = num1 - num2
let multiplicacao = num1 * num2
let divisao = num1 / num2
 
console.log(`Para os valores de entrada ${num1} e ${num2} temos que:`);
console.log(`${num1} + ${num2} = ${soma}`);
console.log(`${num1} - ${num2} = ${subtracao}`);
console.log(`${num1} * ${num2} = ${multiplicacao}`);
console.log(`${num1} / ${num2} = ${divisao}`);