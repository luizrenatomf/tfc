let nota01, nota02, nota03, nota04, notaFinal;

while (true) {
    nota01 = Number(prompt("Informe a primeira nota:"));
    while (nota01 < 0 || nota01 > 15) {
        nota01 = Number(prompt("Nota incorreta. Informe a primera nota novamente:"));
    }
    nota02 = Number(prompt("Informe a segunda nota:"));
    while (nota02 < 0 || nota02 > 25) {
        nota02 = Number(prompt("Nota incorreta. Informe a segunda nota:"));
    }
    nota03 = Number(prompt("Informe a terceira nota:"));
    while (nota03 < 0 || nota03 > 25) {
        nota03 = Number(prompt("Nota incorreta. Informe a terceira nota:"));
    }
    nota04 = Number(prompt("Informe a quarta nota:"));
    while (nota04 < 0 || nota04 > 35) {
        nota04 = Number(prompt("Nota incorreta. Informe a quarta nota:"));
    }    
    notaFinal = nota01 + nota02 + nota03 + nota04; 
    if (notaFinal >= 60) {
        alert("O aluno foi aprovado.");
    } else {
        alert(`O aluno foi reprovado faltando ${60 - notaFinal} pontos para a pontuação mínima de aprovação.`);
    }
    break;
}
