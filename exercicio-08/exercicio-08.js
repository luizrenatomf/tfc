function criaPessoa () {
    let nome = prompt('Insira o nome do adversário:');
    let arma = prompt('Insira a arma do adversário:').toLowerCase();
    let forca = Number(prompt('Insira a força do adversário:'));
    while (forca < 1 || forca > 100) {
        forca = Number(prompt('Valor incorreto. Insira a força do adversário:'));
    }
    return {
        nome: nome,
        arma: arma,
        forca: forca,
        vida: 1800,
    };
}

const arsenal = {
    'martelo de madeira': {
        nome: 'martelo de madeira',
        ataque: 16,
        defesa: 2,
    },
    'florete': {
        nome: 'florete',
        ataque: 10,
        defesa: 7,
    },
    'arco e flechas': {
        nome: 'arco e flechas',
        ataque: 20,
        defesa: 0,
    },
    'varinha de inverno': {
        nome: 'varinha de inverno',
        ataque: 17,
        defesa: 1,
    },
}
       
const adversario01 = criaPessoa();
const adversario02 = criaPessoa();

const potencializador = n => Number(Math.random() * (n - 1) + 1).toFixed(0);
let numero = potencializador(20);

let ataque01 = ((adversario01.forca * numero) + arsenal[adversario01.arma].ataque) - (arsenal[adversario02.arma].defesa * 100);
if (ataque01 < 0) {
    ataque01 = 0;
}
let ataque02 = ((adversario02.forca * numero) + arsenal[adversario02.arma].ataque) - (arsenal[adversario01.arma].defesa * 100);
if (ataque02 < 0) {
    ataque02 = 0;
}
adversario01.vida -= ataque02;
adversario02.vida -= ataque01;

const nomesTitulo = document.getElementById('nomes-titulo');
const texto = document.getElementById('texto');

nomesTitulo.innerHTML = `${adversario01.nome} vs ${adversario02.nome}`;
texto.innerHTML = `<p>${adversario01.nome} perdeu ${ataque02} pontos de vida no ataque de ${adversario02.nome}.</p>`
texto.innerHTML += `<p>${adversario02.nome} perdeu ${ataque01} pontos de vida no ataque de ${adversario01.nome}.</p>`
if (adversario01.vida <= 0 && adversario02.vida <= 0) {
    texto.innerHTML += `<p>Todos morreram.</p>`;
} else if (adversario01.vida <= 0) {
    texto.innerHTML += `${adversario01.nome} morreu.`;
} else if (adversario02.vida <= 0) {
    texto.innerHTML += `${adversario02.nome} morreu.`;
} else {
    texto.innerHTML += `<p>Todos sobreviveram.</p>`;
}